import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'foo',
  pure: false
})
export class FooPipe implements PipeTransform {

  transform(value: string): unknown {
    return value + " FOFO";
  }

}
