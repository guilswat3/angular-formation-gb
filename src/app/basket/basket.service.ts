import { Component, Inject } from '@angular/core';
import { Injectable,  } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { BasketItem } from './basket.types';
import {map, Observable, of, pipe, tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BasketService {

  basket: BasketItem[] = [];

  get total(): number {
    return this.basket.reduce((total, { price }) => total + price, 0);
  }

  get numberOfItems(): number {
    return this.basket.length;
  }

  constructor(private apiService: ApiService) {}

  fetch(): Observable<BasketItem[]> {
    return this.apiService.getBasket().pipe(
      tap((basket) => (this.basket = basket))
    );
  }

  addItem(productId: string): Observable<BasketItem> {
    return this.apiService.addToBasket(productId).pipe(
      tap((basketItem) => {
        this.basket = [...this.basket, basketItem]
      })
    );
  }

}
